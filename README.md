# GIT
## How do we correctly commit a modification?
```sh
$ git commit -m "Fix image version" Dockerfile  # Don't do a git add . before if your file is already on the codebase!!
```

## Your previous commit is crap? Your commit message haz typo? Or you linked the wrong file with the wrong commit message? Just revert the last commit!
```sh
$ git reset --soft HEAD~1
```

# Heroku
## CLI Heroku login
```sh
$ heroku login
```

## Create an Heroku instance
```sh
$ heroku create your-instance-name
```

## How to connect my heroku instance with my git workspace?
```sh
$ heroku git:remote -a your-instance-name
$ git remote -v  # Just to verify that the previous command did it
heroku  https://git.heroku.com/your-instance-name.git (fetch)
heroku  https://git.heroku.com/your-instance-name.git (push)
origin  https://gitlab.com/yourorganization/yourproject.git (fetch)
origin  https://gitlab.com/yourorganization/yourproject.git (push)
```

## Wanna push to your Heroku instance?
```sh
$ git push heroku master
```

## Need your Heroku token to us the API?
```sh
$ heroku auth:token
```

## Want to set or remove a env var on your heroku instance?
```sh
$ heroku config:set APP_LOCATION=HEROKU
$ heroku config:remove APP_LOCATION
```

## Hmmmm Your app is crashing, but why? Just check at the logs
```sh
$ heroku logs --tail
```

# pre-commit hooks
## install-it!
```sh
$ source .venv/bin/active  # Or what ever on Windows
$ pip install pre-commit
```

## Install pre-commit hooks once added to .pre-commit-config.yaml file
```sh
$ pre-commit install
```

## What to try a pre-commit hook before installing it?
```sh
$ pre-commit run flake8 --files main.py
```

#EI

Votre entreprise actuelle, un fabricant de smart TV, vous demande de mettre en place une stratégie de CI/CD afin de mettre en production rapidement leur application web pour smart TV.
Avant de tester grandeur nature votre stratégie, votre cheffe de projet vous demande de faire un projet de test.
Dans un environnement virtuel et avec bottle, faites un projet simple qui s'appellera hello.py et qui retournera "Hello from Smart OS"
Ce que vous demande votre cheffe de projet :
* Avoir 4 branches (develop, master, nightly, release) ;
    - La branche 'develop' sera celle à utiliser pour votre développement ;
    - 'master' c'est votre code testé et validé ;
    - 'nightly', basée sur 'develop', servira pour les partenaires, elle sera tagé 'NIGHTLY-YYYY-MM-DD' toutes les nuits à 00:00 ;
    - Et 'release', basé sur master, tagé 'PROD-YYYWWW' soit 'PROD-Y21W43', tous les lundi à 00:00.
* Faire des tests de lint à chaque commit sur la branche 'develop'.
Elle souhaite que la prod soit déployé (mis à jour) sur Internet à chaque release.

```shell
                ----------------o release
               /
master -------o-------o---------------------o------
                       \                     \
                        -------o develop      ----o nightly
```